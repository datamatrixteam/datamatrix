package de.crazymail.datamatrix.auth.model;

import de.crazymail.datamatrix.model.DatamatrixElement;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
@SequenceGenerator(name="user_seq", sequenceName = "user_seq", initialValue = 5000)
public class User implements DatamatrixElement<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    private Long id;
    private Boolean archived;

    private String userName;
    private String password;

    private String number;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="role_user",
            joinColumns=@JoinColumn(name="user_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="role_id", referencedColumnName="id"))
    private List<Role> roles;

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsArchived() {
        return this.archived;
    }

    public void setIsArchived(Boolean archived) {
        this.archived = archived;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role r) {
        this.roles.add(r);
    }

    public Set<String> getRolesAsSet() {
        Set<String> roles = new HashSet<>();
        for(Role r : this.roles)
            roles.add(r.getRoleName());

        return roles;
    }
}
