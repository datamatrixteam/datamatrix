package de.crazymail.datamatrix.auth.manager.impl;

import de.crazymail.datamatrix.auth.exception.InvalidPasswordException;
import de.crazymail.datamatrix.auth.exception.InvalidUserNameException;
import de.crazymail.datamatrix.auth.exception.MultipleUserException;
import de.crazymail.datamatrix.auth.manager.UserManager;
import de.crazymail.datamatrix.auth.model.Role;
import de.crazymail.datamatrix.auth.model.User;
import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.database.impl.DataManagerBean;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Stateless
public class UserManagerBean implements UserManager {

    @EJB
    private DataManager<User, Long> dataManager;

    @PostConstruct
    public void init() {
        this.dataManager.setClazz(User.class);
    }

    @Override
    public User create() {
        User user = new User();
        user.setRoles(new ArrayList<Role>());
        user.setIsArchived(Boolean.FALSE);
        return user;
    }

    @Override
    public boolean validatePassword(User u, String password) throws InvalidPasswordException {
        if(password == null || password.isEmpty())
            throw new InvalidPasswordException();

        if(u.getPassword().equals(password))
            return true;

        throw new InvalidPasswordException();
    }

    @Override
    public User getByUserName(final String username) throws MultipleUserException, InvalidUserNameException {
        List<User> lst = this.dataManager.getByQuery("select u from User u where userName = :name", new HashMap<String, String>(){{
            put("name", username);
        }});

        if(lst.size() == 0)
            throw new InvalidUserNameException(username);

        if(lst.size() > 1)
            throw new MultipleUserException(username);

        return lst.get(0);
    }

    @Override
    public User save(User u) {
        return this.dataManager.saveWithoutSequence(u);
    }

}
