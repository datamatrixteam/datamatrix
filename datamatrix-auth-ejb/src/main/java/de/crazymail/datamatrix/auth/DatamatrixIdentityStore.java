package de.crazymail.datamatrix.auth;


import de.crazymail.datamatrix.auth.exception.InvalidPasswordException;
import de.crazymail.datamatrix.auth.exception.InvalidUserNameException;
import de.crazymail.datamatrix.auth.exception.MultipleUserException;
import de.crazymail.datamatrix.auth.manager.UserManager;
import de.crazymail.datamatrix.auth.model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.util.HashSet;

import static java.util.Arrays.asList;
import static javax.security.enterprise.identitystore.CredentialValidationResult.INVALID_RESULT;

@ApplicationScoped
public class DatamatrixIdentityStore implements IdentityStore {

    @Inject
    private UserManager userManager;

    public CredentialValidationResult validate(UsernamePasswordCredential userCredential) {

        try {
            User u = this.userManager.getByUserName(userCredential.getCaller());
            if(this.userManager.validatePassword(u, userCredential.getPasswordAsString()))
                return new CredentialValidationResult(userCredential.getCaller(), u.getRolesAsSet());

        } catch (MultipleUserException e) {

        } catch (InvalidUserNameException e) {

        } catch (InvalidPasswordException e) {

        }


        /*
        if (userCredential.compareTo("admin", "pwd1")) {
            return new CredentialValidationResult("admin",
                    new HashSet<>(asList("admin", "user", "demo")));
        }
        */
        return INVALID_RESULT;
    }

}
