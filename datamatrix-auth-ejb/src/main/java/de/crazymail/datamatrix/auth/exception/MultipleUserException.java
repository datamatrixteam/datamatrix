package de.crazymail.datamatrix.auth.exception;

public class MultipleUserException extends Exception {
    public MultipleUserException(String username) {
        super(username);
    }
}
