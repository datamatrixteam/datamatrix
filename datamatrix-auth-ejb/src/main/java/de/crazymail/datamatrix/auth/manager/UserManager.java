package de.crazymail.datamatrix.auth.manager;

import de.crazymail.datamatrix.auth.exception.InvalidPasswordException;
import de.crazymail.datamatrix.auth.exception.InvalidUserNameException;
import de.crazymail.datamatrix.auth.exception.MultipleUserException;
import de.crazymail.datamatrix.auth.model.User;

import javax.ejb.Local;

@Local
public interface UserManager {

    User getByUserName(String username) throws MultipleUserException, InvalidUserNameException;

    User create();

    boolean validatePassword(User u, String password) throws InvalidPasswordException;

    User save(User u);
}
