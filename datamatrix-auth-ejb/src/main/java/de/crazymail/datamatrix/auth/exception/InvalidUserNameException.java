package de.crazymail.datamatrix.auth.exception;

public class InvalidUserNameException extends Exception {
    public InvalidUserNameException(String username) {
        super(username);
    }
}
