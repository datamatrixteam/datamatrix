package de.crazymail.datamatrix.model;

public interface DatamatrixElement<I> {

    I getId();
    void setId(I id);

    String getNumber();
    void setNumber(String number);

    Boolean getIsArchived();
    void setIsArchived(Boolean archived);

}
