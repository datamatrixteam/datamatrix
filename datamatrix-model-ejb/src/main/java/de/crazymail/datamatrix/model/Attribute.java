package de.crazymail.datamatrix.model;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(name = "attribute_seq", sequenceName = "attribute_seq", initialValue = 5000)
public class Attribute implements DatamatrixElement<Long> {

    @Id
    @GeneratedValue(generator = "attribute_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String number;
    private String name;
    private String description;
    private Boolean archived;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsArchived() {
        return this.archived;
    }

    public void setIsArchived(Boolean isArchived) {
        this.archived = isArchived;
    }

    /*
    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }
    */
}
