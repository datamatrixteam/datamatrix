package de.crazymail.datamatrix.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "mytype")
@SequenceGenerator(name = "type_seq", sequenceName = "type_seq", initialValue = 5000)

@NamedQueries({
    @NamedQuery(name = "getRootTypes", query = "select t from Type t where t.parent = null and t.archived = false")
})

public class Type implements DatamatrixElement<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "type_seq")
    private Long id;
    private Boolean archived;
    private String number;
    private String name;
    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="type_attribute",
            joinColumns=@JoinColumn(name="type_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="attribute_id", referencedColumnName="id"))
    @Fetch(value = FetchMode.SUBSELECT) // Hibernate only
    private List<Attribute> attributes;

    @ManyToOne(fetch = FetchType.EAGER)
    private Type parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT) // Hibernate only
    private List<Type> children;

    public Type getParent() {
        return parent;
    }

    public void setParent(Type parent) {
        this.parent = parent;
    }

    public List<Type> getChildren() {
        return children;
    }

    public void setChildren(List<Type> children) {
        this.children = children;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsArchived() {
        return this.archived;
    }

    public void setIsArchived(Boolean archived) {
        this.archived = archived;
    }

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }
    
    public void addAttribute(Attribute attr) {
    	this.attributes.add(attr);
    }
    
    public void addAttributes(List<Attribute> attributes) {
    	this.attributes.addAll(attributes);
    }


}
