package de.crazymail.datamtrix;

import de.crazymail.datamatrix.model.Attribute;
import de.crazymail.datamatrix.model.Type;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TypeTest {

    @Test
    public void testConstructor() {
        Type type = new Type();

        Assert.assertNotNull(type);
        Assert.assertNull(type.getId());
        Assert.assertNull(type.getName());
        Assert.assertNull(type.getNumber());
        Assert.assertNull(type.getDescription());
        Assert.assertNull(type.getIsArchived());
        Assert.assertNull(type.getParent());
        Assert.assertNull(type.getChildren());
        Assert.assertNull(type.getAttributes());
    }

    @Test
    public void testAddAttribute() {
        Type type = new Type();
        type.setAttributes(new ArrayList<>());

        Attribute attribute = new Attribute();
        attribute.setId(1000L);

        type.addAttribute(attribute);

        Assert.assertNotNull(type.getAttributes());
        Assert.assertEquals(1, type.getAttributes().size());
        Assert.assertEquals(1000, type.getAttributes().get(0).getId().longValue());
    }

    @Test
    public void testAddAttributes() {

        Type type = new Type();
        type.setAttributes(new ArrayList<>());

        Attribute attribute1 = new Attribute();
        attribute1.setId(1001L);

        Attribute attribute2 = new Attribute();
        attribute2.setId(1002L);

        Attribute attribute3 = new Attribute();
        attribute3.setId(1003L);

        List<Attribute> attributes = new ArrayList<Attribute>() {{
           add(attribute1);
           add(attribute2);
           add(attribute3);
        }};

        type.addAttributes(attributes);

        Assert.assertNotNull(type.getAttributes());
        Assert.assertEquals(3, type.getAttributes().size());

        Assert.assertEquals(1001, type.getAttributes().get(0).getId().longValue());
        Assert.assertEquals(1002, type.getAttributes().get(1).getId().longValue());
        Assert.assertEquals(1003, type.getAttributes().get(2).getId().longValue());

    }

}
