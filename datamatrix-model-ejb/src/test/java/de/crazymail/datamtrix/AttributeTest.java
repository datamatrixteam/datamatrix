package de.crazymail.datamtrix;

import de.crazymail.datamatrix.model.Attribute;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class AttributeTest {

    @Test
    public void testConstructor() {
        Attribute attribute = new Attribute();
        Assert.assertNotNull(attribute);
        Assert.assertNull(attribute.getId());
        Assert.assertNull(attribute.getName());
        Assert.assertNull(attribute.getNumber());
        Assert.assertNull(attribute.getDescription());
        Assert.assertNull(attribute.getIsArchived());
    }
}
