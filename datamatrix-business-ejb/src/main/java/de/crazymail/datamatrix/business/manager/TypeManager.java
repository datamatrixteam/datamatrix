package de.crazymail.datamatrix.business.manager;

import de.crazymail.datamatrix.model.Attribute;
import de.crazymail.datamatrix.model.Type;

import javax.ejb.Local;
import java.util.List;

@Local
public interface TypeManager extends GenericManager<Type, Long> {

    Type create(Type parent);

    Type addAttribute(Type type, Attribute attribute);

    Type addAttributes(Type type, List<Attribute> selectedAttributesToAdd);
    List<Type> getRootTypes();
    List<Attribute> getAttributesEffective(Type t);
}
