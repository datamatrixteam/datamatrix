package de.crazymail.datamatrix.business.manager;

import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.model.DatamatrixElement;

import java.util.List;

public interface GenericManager<T extends DatamatrixElement, I> {
    T get(I id);
    T save(T obj) throws InvalidSequenceException;

    T create();

    List<T> getAll();

    T archive(T attribute) throws InvalidSequenceException;
}
