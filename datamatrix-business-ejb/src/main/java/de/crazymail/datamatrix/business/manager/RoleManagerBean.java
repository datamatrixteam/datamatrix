package de.crazymail.datamatrix.business.manager;

import de.crazymail.datamatrix.auth.model.Role;
import de.crazymail.datamatrix.business.manager.RoleManager;
import de.crazymail.datamatrix.business.manager.impl.GenericManagerBean;
import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class RoleManagerBean implements RoleManager {

    @EJB
    private DataManager<Role, Long> dataManager;

    @PostConstruct
    public void init() {
        this.dataManager.setClazz(RoleManagerBean.class);
    }

    @Override
    public Role create() {
        Role role = new Role();
        role.setIsArchived(Boolean.FALSE);
        return role;
    }

    public Role get(Long id) {
        return (Role) this.dataManager.get(id);
    }

    @Override
    public Role save(Role role) throws InvalidSequenceException {
        return (Role) this.dataManager.save(role);
    }
}
