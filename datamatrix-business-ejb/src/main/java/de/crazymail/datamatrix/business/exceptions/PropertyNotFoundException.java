package de.crazymail.datamatrix.business.exceptions;

public class PropertyNotFoundException extends Exception {
    public PropertyNotFoundException(String key1, String key2, String key3) {
        super("Property not found: keys: " + key1 + ", " + key2 + ", " + key3);
    }
}
