package de.crazymail.datamatrix.business.manager;

import de.crazymail.datamatrix.model.Attribute;

import javax.ejb.Local;

@Local
public interface AttributeManager extends GenericManager<Attribute, Long> {
}
