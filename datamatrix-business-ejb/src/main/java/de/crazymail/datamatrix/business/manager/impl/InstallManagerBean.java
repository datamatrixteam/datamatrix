package de.crazymail.datamatrix.business.manager.impl;

import de.crazymail.datamatrix.auth.manager.UserManager;
import de.crazymail.datamatrix.auth.model.Role;
import de.crazymail.datamatrix.auth.model.User;
import de.crazymail.datamatrix.business.manager.RoleManager;
import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.business.manager.InstallManager;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.model.DatamatrixElement;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class InstallManagerBean extends GenericManagerBean implements InstallManager {

    @EJB
    private DataManager dataManager;
    @EJB
    private RoleManager roleManager;
    @EJB
    private UserManager userManager;

    /*
    @EJB
    private PropertyManagerBean propertyManager;
    */

    @Override
    public DataManager getDataManager() {
        return dataManager;
    }

    @PostConstruct
    public void init() {
        infoMessage("init execution");
        try {
            Role adminRole = this.createRole(1000L, "admin", "Admins can do all things");
            this.createRole(1001L, "poweruser", "PowerUser");
            this.createRole(1002L, "user", "User");

            this.createAdminUser(adminRole);
        } catch (InvalidSequenceException ex) {
            ex.printStackTrace();
        }
        /*
        Property sequenceGeneratorAttributeUsage = this.propertyManager.create("SequenceGenerator","Attribute", "usage", "true");
        sequenceGeneratorAttributeUsage = this.propertyManager.save(sequenceGeneratorAttributeUsage);
        Property sequenceGeneratorAttributePrefix = this.propertyManager.create("SequenceGenerator", "Attribute", "Prefix", "ATTR");
        sequenceGeneratorAttributePrefix = this.propertyManager.save(sequenceGeneratorAttributePrefix);
        Property sequenceGeneratorAttributeSuffix = this.propertyManager.create("SequenceGenerator", "Attribute", "Suffix", "X");
        sequenceGeneratorAttributeSuffix = this.propertyManager.save(sequenceGeneratorAttributeSuffix);
        Property sequenceGeneratorAttributeDigits = this.propertyManager.create("SequenceGenerator", "Attribute", "Digits", "3");
        sequenceGeneratorAttributeDigits = this.propertyManager.save(sequenceGeneratorAttributeDigits);
        */
    }

    @Override
    public DatamatrixElement create() {
        return null;
    }

    private Role createRole(Long id, String name, String description) throws InvalidSequenceException {
        Role r = this.roleManager.create();
        //r.setId(id);
        r.setRoleName(name);
        r.setDescription(description);
        r.setEnabled(Boolean.TRUE);
        r = this.roleManager.save(r);
        return r;
    }

    private void createAdminUser(Role adminRole) throws InvalidSequenceException {
        User adminUser = this.userManager.create();
        //adminRole.setId(1000L);
        adminUser.setUserName("admin");
        adminUser.setPassword("W1s6!F=tmc");
        adminUser.addRole(adminRole);
        adminUser = this.userManager.save(adminUser);
    }

}
