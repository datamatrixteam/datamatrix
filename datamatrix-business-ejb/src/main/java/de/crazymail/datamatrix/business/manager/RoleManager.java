package de.crazymail.datamatrix.business.manager;


import de.crazymail.datamatrix.auth.model.Role;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;

import javax.ejb.Local;

@Local
public interface RoleManager {
    Role create();

    Role save(Role role) throws InvalidSequenceException;
}
