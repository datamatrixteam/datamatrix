package de.crazymail.datamatrix.business.manager.impl;

import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.business.manager.TypeManager;
import de.crazymail.datamatrix.model.Attribute;
import de.crazymail.datamatrix.model.Type;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class TypeManagerBean extends GenericManagerBean<Type, Long> implements TypeManager {

    @EJB
    private DataManager<Type, Long> dataManager;

    @PostConstruct
    public void init() {
        this.dataManager.setClazz(Type.class);
    }

    @Override
    public Type create() {
        Type type = new Type();
        type.setIsArchived(Boolean.FALSE);
        type.setAttributes(new ArrayList<>());
        type.setChildren(new ArrayList<>());
        return type;
    }

    @Override
    public Type create(Type parent) {
        Type t = this.create();
        t.setParent(parent);
        t.setAttributes(new ArrayList<>());
        t.setChildren(new ArrayList<>());
        parent.getChildren().add(t);
        return t;
    }

    @Override
    public DataManager<Type, Long> getDataManager() {
        return this.dataManager;
    }

    /*
    public Type archive(Type type) throws InvalidSequenceException {
        type.setIsArchived(Boolean.TRUE);
        return this.save(type);
    }
    */

    private Boolean hasAttribute(Type t, Attribute attr) {
        for(Attribute a : t.getAttributes())
            if(a.getId().equals(attr.getId()))
                return Boolean.TRUE;

        return Boolean.FALSE;
    }

    @Override
    public Type addAttribute(Type type, Attribute attribute) {
        if(!this.hasAttribute(type, attribute))
            type.addAttribute(attribute);

        return type;
    }

    @Override
    public Type addAttributes(Type type, List<Attribute> attributes) {
        for(Attribute attribute : attributes)
            type = this.addAttribute(type, attribute);

        return type;
    }

    @Override
    public List<Type> getRootTypes() {
        return this.dataManager.namedQuery(Type.class, "getRootTypes");
    }

    public List<Attribute> getAttributesEffective(Type t) {

        List<Attribute> attributes = new ArrayList<>();
        this.getAttributes(t, attributes);
        return attributes;
    }

    private void getAttributes(Type t, List<Attribute> attributes) {
        attributes.addAll(t.getAttributes());
        if(t.getParent() != null) {
            Type parent = t.getParent();
            this.getAttributes(parent, attributes);
        }
    }
}
