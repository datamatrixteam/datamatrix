package de.crazymail.datamatrix.business.manager.impl;

import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.business.manager.GenericManager;
import de.crazymail.datamatrix.model.DatamatrixElement;

import java.util.List;
import java.util.logging.Logger;

public abstract class GenericManagerBean<T extends DatamatrixElement, I> implements GenericManager<T, I> {

    public abstract DataManager<T, I> getDataManager();

    private Logger logger = Logger.getLogger(this.getClass().getSimpleName());

    @Override
    public T get(I id) {
        return this.getDataManager().get(id);
    }

    @Override
    public T save(T obj) throws InvalidSequenceException {
        return this.getDataManager().save(obj);
    }

    @Override
    public List<T> getAll() {
        return this.getDataManager().getAll();
    }

    public void infoMessage(String message) {
        this.logger.info(message);
    }

    @Override
    public T archive(T attribute) throws InvalidSequenceException {
        attribute.setIsArchived(Boolean.TRUE);
        return this.save(attribute);
    }
}
