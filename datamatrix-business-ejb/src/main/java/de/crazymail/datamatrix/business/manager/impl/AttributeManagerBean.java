package de.crazymail.datamatrix.business.manager.impl;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

import de.crazymail.datamatrix.business.manager.AttributeManager;
import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.model.Attribute;

@Stateless
public class AttributeManagerBean extends GenericManagerBean<Attribute, Long> implements AttributeManager {

    @EJB
    private DataManager dataManager;

    @PostConstruct
    public void init() {
        this.dataManager.setClazz(Attribute.class);
    }

    @Override
    public DataManager<Attribute, Long> getDataManager() {
        return this.dataManager;
    }

    @Override
    public Attribute create() {
        Attribute attr = new Attribute();
        attr.setIsArchived(Boolean.FALSE);
        return attr;
    }

    /*
    public Attribute get(Long id) {
        return (Attribute) this.dataManager.get(id);
    }
    */

    /*
    public List<Attribute> get(Long[] attributeIds) {
        List<Attribute> attributeList = new ArrayList<>();
        for(Long id : attributeIds)
            attributeList.add(this.get(id));
        return attributeList;
    }
    */

}
