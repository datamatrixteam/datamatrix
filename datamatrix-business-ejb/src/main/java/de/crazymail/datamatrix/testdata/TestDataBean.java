package de.crazymail.datamatrix.testdata;

import de.crazymail.datamatrix.business.manager.impl.GenericManagerBean;
import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.database.Sequence;
import de.crazymail.datamatrix.database.SequenceManager;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.business.manager.AttributeManager;
import de.crazymail.datamatrix.testdata.TestData;
import de.crazymail.datamatrix.business.manager.TypeManager;
import de.crazymail.datamatrix.model.Attribute;
import de.crazymail.datamatrix.model.DatamatrixElement;
import de.crazymail.datamatrix.model.Type;

import javax.annotation.PostConstruct;
import javax.ejb.*;

@Singleton
@DependsOn("InstallManagerBean")
@Startup
public class TestDataBean extends GenericManagerBean implements TestData {

    @EJB
    private AttributeManager attributeManager;
    @EJB
    private TypeManager typeManager;
    @EJB
    private SequenceManager sequenceManager;

    @Override
    public DataManager<DatamatrixElement<Long>, Long> getDataManager() {
        return null;
    }

    @PostConstruct
    public void init() {

        try {

            Sequence seqAttribute = this.sequenceManager.create(Attribute.class.getSimpleName(), "ATTR", "X", 5, 100);
            seqAttribute = this.sequenceManager.save(seqAttribute);
            this.sequenceManager.addSequence(seqAttribute);
            Sequence seqType = this.sequenceManager.create(Type.class.getSimpleName(), "T", "Y", 4, 200);
            seqType = this.sequenceManager.save(seqType);
            this.sequenceManager.addSequence(seqType);

            Attribute attributeColor = this.attributeManager.create();
            attributeColor.setNumber("001");
            attributeColor.setName("Color");
            attributeColor = this.attributeManager.save(attributeColor);

            Attribute attributeMaxVolume = this.attributeManager.create();
            attributeMaxVolume.setNumber("002");
            attributeMaxVolume.setName("Max Volume");
            attributeMaxVolume = this.attributeManager.save(attributeMaxVolume);

            Attribute attributeOwner = this.attributeManager.create();
            attributeOwner.setNumber("003");
            attributeOwner.setName("Owner");
            attributeOwner = this.attributeManager.save(attributeOwner);

            Attribute attributeHasLink = this.attributeManager.create();
            attributeHasLink.setNumber("004");
            attributeHasLink.setName("Has a link");
            attributeHasLink = this.attributeManager.save(attributeHasLink);

            Attribute attributeIsConsumable = this.attributeManager.create();
            attributeIsConsumable.setNumber("005");
            attributeIsConsumable.setName("Is a consumable");
            attributeIsConsumable = this.attributeManager.save(attributeIsConsumable);

            Attribute attributeFlavor = this.attributeManager.create();
            attributeFlavor.setNumber("006");
            attributeFlavor.setName("Flavor");
            attributeFlavor = this.attributeManager.save(attributeFlavor);

            Attribute attributeGranularity = this.attributeManager.create();
            attributeGranularity.setNumber("007");
            attributeGranularity.setName("Granularity");
            attributeGranularity = this.attributeManager.save(attributeGranularity);

            Attribute attributeBasicMaterial = this.attributeManager.create();
            attributeBasicMaterial.setNumber("008");
            attributeBasicMaterial.setName("Basic Material");
            attributeBasicMaterial = this.attributeManager.save(attributeBasicMaterial);

            Attribute attributeSortOfGlass = this.attributeManager.create();
            attributeSortOfGlass.setNumber("009");
            attributeSortOfGlass.setName("Sort of Glass");
            attributeSortOfGlass = this.attributeManager.save(attributeSortOfGlass);

            Attribute attributeWasteType = this.attributeManager.create();
            attributeWasteType.setNumber("010");
            attributeWasteType.setName("Waste Type");
            attributeWasteType = this.attributeManager.save(attributeWasteType);

            Attribute attributeViscosity = this.attributeManager.create();
            attributeViscosity.setNumber("011");
            attributeViscosity.setName("Viscosity");
            attributeViscosity = this.attributeManager.save(attributeViscosity);

            Type typeContainer = this.typeManager.create();
            typeContainer.setNumber("001");
            typeContainer.setName("Container");
            typeContainer.addAttribute(attributeMaxVolume);
            typeContainer.addAttribute(attributeIsConsumable);
            typeContainer.addAttribute(attributeBasicMaterial);

            typeContainer = this.typeManager.save(typeContainer);

            Type typeVolumetrikFlask = this.typeManager.create();
            typeVolumetrikFlask.setParent(typeContainer);
            typeVolumetrikFlask.setNumber("002");
            typeVolumetrikFlask.setName("Volumetrik Flask");
            typeVolumetrikFlask.addAttribute(attributeSortOfGlass);
            typeVolumetrikFlask = this.typeManager.save(typeVolumetrikFlask);

            Type typeRecycleBin = this.typeManager.create();
            typeRecycleBin.setParent(typeContainer);
            typeRecycleBin.setNumber("003");
            typeRecycleBin.setName("Bin");
            typeRecycleBin.addAttribute(attributeWasteType);
            typeRecycleBin = this.typeManager.save(typeRecycleBin);


            Type typeSubstance = this.typeManager.create();
            typeSubstance.setNumber("004");
            typeSubstance.setName("Substance");
            typeSubstance.addAttribute(attributeColor);
            typeSubstance.addAttribute(attributeFlavor);
            typeSubstance = this.typeManager.save(typeSubstance);

            Type typeSubstancePowder = this.typeManager.create();
            typeSubstancePowder.setNumber("005");
            typeSubstancePowder.setName("Powder");
            typeSubstancePowder.setParent(typeSubstance);
            typeSubstancePowder.addAttribute(attributeGranularity);
            typeSubstancePowder = this.typeManager.save(typeSubstancePowder);

            Type typeSubstanceGel = this.typeManager.create();
            typeSubstanceGel.setNumber("006");
            typeSubstanceGel.setName("Gel");
            typeSubstanceGel.setParent(typeSubstance);
            typeSubstanceGel.addAttribute(attributeViscosity);
            typeSubstanceGel = this.typeManager.save(typeSubstanceGel);
        } catch (InvalidSequenceException ex) {
            this.infoMessage(ex.getMessage());
        }
    }

    @Override
    public DatamatrixElement create() {
        return null;
    }
}
