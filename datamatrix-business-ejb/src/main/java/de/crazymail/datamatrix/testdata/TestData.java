package de.crazymail.datamatrix.testdata;

import de.crazymail.datamatrix.business.manager.GenericManager;

import javax.ejb.Local;

@Local
public interface TestData extends GenericManager {
}
