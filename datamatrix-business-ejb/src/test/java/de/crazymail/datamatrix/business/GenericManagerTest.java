package de.crazymail.datamatrix.business;

import de.crazymail.datamatrix.business.manager.GenericManager;
import de.crazymail.datamatrix.business.manager.impl.GenericManagerBean;
import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.database.Sequence;
import de.crazymail.datamatrix.database.SequenceManager;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.database.exception.MultipleSequencesFoundException;
import de.crazymail.datamatrix.database.exception.NoSequenceFoundException;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.impl.base.filter.ExcludeRegExpPaths;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.logging.Logger;

@RunWith(Arquillian.class)
@Ignore
public class GenericManagerTest {

    private Logger log = Logger.getLogger(GenericManagerTest.class.getSimpleName());

    @Inject
    public DataManager<TestEntity, Long> dataManager;
    @Inject
    private SequenceManager sequenceManager;

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive archive = ShrinkWrap.create(JavaArchive.class)
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.model")
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.database")
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.business")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("test-persistence.xml", "persistence.xml");
        return archive;
    }

    @Before
    public void init() {
        log.info("init execution");
        this.dataManager.setClazz(TestEntity.class);

        try {
            Sequence seq = this.sequenceManager.getSequenceByEntity(TestEntity.class.getSimpleName());
            log.info("sequence found");
        } catch (NoSequenceFoundException e) {
            log.info("no sequence, creating...");
            Sequence seq = sequenceManager.create(TestEntity.class.getSimpleName(), "TE", "E", 4, 1);
            seq = sequenceManager.save(seq);
            sequenceManager.addSequence(seq);
            log.info("done");
        } catch (MultipleSequencesFoundException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class TestManager extends GenericManagerBean<TestEntity, Long> implements GenericManager<TestEntity, Long> {

        public TestEntity create() {
            return new TestEntity();
        }

        public DataManager getDataManager() {
            return dataManager;
        }

    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testGetAndSave() throws InvalidSequenceException {
        log.info("testGetAndSave execution");
        TestManager tm = new TestManager();

        TestEntity e = new TestEntity();
        e.setIsArchived(Boolean.FALSE);
        e.setName("test e");

        e = tm.save(e);
        Assert.assertNotNull(e);
        Assert.assertNotNull(e.getId());
        Assert.assertFalse(e.getIsArchived());

        Assert.assertNotNull(e.getName());
        Assert.assertEquals("test e", e.getName());

        Long entityId = e.getId();

        e = null;
        e = tm.get(entityId);

        Assert.assertNotNull(e);
        Assert.assertNotNull(e.getId());
        Assert.assertFalse(e.getIsArchived());
        Assert.assertNotNull(e.getName());
        Assert.assertEquals("test e", e.getName());
    }
}
