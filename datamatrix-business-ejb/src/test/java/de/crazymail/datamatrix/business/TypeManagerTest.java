package de.crazymail.datamatrix.business;

import de.crazymail.datamatrix.business.manager.AttributeManager;
import de.crazymail.datamatrix.business.manager.TypeManager;
import de.crazymail.datamatrix.database.Sequence;
import de.crazymail.datamatrix.database.SequenceManager;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.database.exception.MultipleSequencesFoundException;
import de.crazymail.datamatrix.database.exception.NoSequenceFoundException;
import de.crazymail.datamatrix.model.Attribute;
import de.crazymail.datamatrix.model.Type;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.impl.base.filter.ExcludeRegExpPaths;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.w3c.dom.Attr;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

@RunWith(Arquillian.class)
public class TypeManagerTest {

    @Inject
    private TypeManager typeManager;
    @Inject
    private AttributeManager attributeManager;
    @Inject
    private SequenceManager sequenceManager;

    private Logger log = Logger.getLogger(TypeManagerTest.class.getSimpleName());

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive archive = ShrinkWrap.create(JavaArchive.class)
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.model")
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.database")
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.business")
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.auth")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("test-persistence.xml", "persistence.xml");
        return archive;
    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testCreateWithoutParent() {
        Type t = typeManager.create();

        Assert.assertNotNull(t);
        Assert.assertNull(t.getId());
        Assert.assertNull(t.getParent());
        Assert.assertNotNull(t.getIsArchived());
        Assert.assertFalse(t.getIsArchived());
        Assert.assertNull(t.getNumber());
        Assert.assertNull(t.getName());
        Assert.assertNull(t.getDescription());
        Assert.assertNotNull(t.getAttributes());
        Assert.assertEquals(0, t.getAttributes().size());
        Assert.assertNotNull(t.getChildren());
        Assert.assertEquals(0, t.getChildren().size());

    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testCreateWithParent() {
        Type parent = this.typeManager.create();
        parent.setName("Parent");

        Type t = this.typeManager.create(parent);
        Assert.assertNotNull(t);
        Assert.assertNull(t.getId());
        Assert.assertNotNull(t.getParent());
        Assert.assertEquals(parent.toString(), t.getParent().toString());
        Assert.assertNotNull(t.getIsArchived());
        Assert.assertFalse(t.getIsArchived());
        Assert.assertNull(t.getNumber());
        Assert.assertNull(t.getName());
        Assert.assertNull(t.getDescription());
        Assert.assertNotNull(t.getAttributes());
        Assert.assertEquals(0, t.getAttributes().size());
        Assert.assertNotNull(t.getChildren());
        Assert.assertEquals(0, t.getChildren().size());

        Assert.assertNotNull(parent.getChildren());
        Assert.assertEquals(1, parent.getChildren().size());
        Assert.assertEquals(parent.getChildren().get(0).toString(), t.toString());
    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testGetDataManager() {
        // TODO: Need a way to test getDataManager();
    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testAddAttributes() throws InvalidSequenceException {
        List<Attribute> lstAttributes = this.persistAttributes(new ArrayList<String>() {{
            add("Color");
            add("Size");
            add("Width");
            add("Height");
        }});

        Type t = this.typeManager.create();
        this.typeManager.addAttributes(t, lstAttributes);

        Assert.assertNotNull(t);
        Assert.assertNotNull(t.getAttributes());
        Assert.assertEquals(lstAttributes.size(), t.getAttributes().size());
        Assert.assertEquals("Color", t.getAttributes().get(0).getName());
        Assert.assertEquals("Size", t.getAttributes().get(1).getName());
        Assert.assertEquals("Width", t.getAttributes().get(2).getName());
        Assert.assertEquals("Height", t.getAttributes().get(3).getName());
    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testAddAttribute() throws InvalidSequenceException {
        List<Attribute> lstAttributes = this.persistAttributes(new ArrayList<String>() {{
            add("Color");
            add("Size");
            add("Width");
            add("Height");
        }});

        Type t = this.typeManager.create();
        //this.typeManager.addAttributes(t, lstAttributes);
        for(Attribute attr : lstAttributes)
            t = this.typeManager.addAttribute(t, attr);

        Assert.assertNotNull(t);
        Assert.assertNotNull(t.getAttributes());
        Assert.assertEquals(lstAttributes.size(), t.getAttributes().size());
        Assert.assertEquals("Color", t.getAttributes().get(0).getName());
        Assert.assertEquals("Size", t.getAttributes().get(1).getName());
        Assert.assertEquals("Width", t.getAttributes().get(2).getName());
        Assert.assertEquals("Height", t.getAttributes().get(3).getName());
    }

    /*
    @Test
    public void testHasAttribute() throws InvalidSequenceException {
        List<Attribute> lstAttributes = this.persistAttributes(new ArrayList<String>() {{
            add("Color");
            add("Size");
            add("Width");
            add("Height");
        }});

        Type t = this.typeManager.create();
        this.typeManager.addAttributes(t, lstAttributes);

        Attribute attrMisc = this.attributeManager.create();
        attrMisc.setName("misc");


    }
    */

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testGetRootTypes() throws InvalidSequenceException {
        List<Type> typesWithoutParent = this.persistTypesWithoutParent(new ArrayList<String>() {{
            add("Type 1");
            add("Type 2");
            add("Type 3");
        }});

        Type parent = this.typeManager.create();
        parent.setName("Parent");
        parent = this.typeManager.save(parent);

        List<Type> typesWithParent = this.persistTypesWithParent(new ArrayList<String>() {{
            add("Type 4");
            add("Type 5");
        }}, parent);

        List<Type> lst = this.typeManager.getRootTypes();
        for(Type t : lst) {
            log.info("Type: " + t.getName());
        }
        Assert.assertNotNull(lst);
        Assert.assertEquals(4, lst.size());

        // TODO: test if type is #1, #2 and #3

    }

    private List<Type> persistTypesWithoutParent(List<String> names) throws InvalidSequenceException {
        try {
            Sequence seq = this.sequenceManager.getSequenceByEntity(Type.class.getSimpleName());
        } catch (NoSequenceFoundException e) {
            Sequence seq = this.sequenceManager.create(Type.class.getSimpleName(), "T", "Y", 5, 100);
            seq = this.sequenceManager.save(seq);
            this.sequenceManager.addSequence(seq);
        } catch (MultipleSequencesFoundException e) {
            this.sequenceManager.deleteAll();
            this.sequenceManager.clearCache();
            Sequence seq = this.sequenceManager.create(Type.class.getSimpleName(), "T", "Y", 5, 100);
            seq = this.sequenceManager.save(seq);
            this.sequenceManager.addSequence(seq);
        } finally {
            List<Type> lst = new ArrayList<>();

            for(String name : names) {
                Type type = this.typeManager.create();
                type.setName(name);
                lst.add(this.typeManager.save(type));
            }

            return lst;
        }
    }

    private List<Type> persistTypesWithParent(List<String> names, Type parent) throws InvalidSequenceException {
        try {
            Sequence seq = this.sequenceManager.getSequenceByEntity(Type.class.getSimpleName());
        } catch (NoSequenceFoundException e) {
            Sequence seq = this.sequenceManager.create(Type.class.getSimpleName(), "T", "Y", 5, 100);
            seq = this.sequenceManager.save(seq);
            this.sequenceManager.addSequence(seq);
        } catch (MultipleSequencesFoundException e) {
            this.sequenceManager.deleteAll();
            this.sequenceManager.clearCache();
            Sequence seq = this.sequenceManager.create(Type.class.getSimpleName(), "T", "Y", 5, 100);
            seq = this.sequenceManager.save(seq);
            this.sequenceManager.addSequence(seq);
        } finally {
            List<Type> lst = new ArrayList<>();

            for(String name : names) {
                Type type = this.typeManager.create(parent);
                type.setName(name);
                lst.add(this.typeManager.save(type));
            }

            return lst;
        }
    }

    private List<Attribute> persistAttributes(List<String> names) throws InvalidSequenceException {

        try {
            Sequence seq = this.sequenceManager.getSequenceByEntity(Attribute.class.getSimpleName());
        } catch (NoSequenceFoundException e) {
            Sequence seq = this.sequenceManager.create(Attribute.class.getSimpleName(), "ATTR", "X", 5, 100);
            seq = this.sequenceManager.save(seq);
            this.sequenceManager.addSequence(seq);
        } catch (MultipleSequencesFoundException e) {
            this.sequenceManager.deleteAll();
            this.sequenceManager.clearCache();
            Sequence seq = this.sequenceManager.create(Attribute.class.getSimpleName(), "ATTR", "X", 5, 100);
            seq = this.sequenceManager.save(seq);
            this.sequenceManager.addSequence(seq);
        } finally {
            List<Attribute> lst = new ArrayList<>();

            for(String name : names) {
                Attribute attr = this.attributeManager.create();
                attr.setName(name);
                lst.add(this.attributeManager.save(attr));
            }

            return lst;
        }
    }

}
