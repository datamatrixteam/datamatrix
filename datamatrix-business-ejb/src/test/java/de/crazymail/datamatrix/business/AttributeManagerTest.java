package de.crazymail.datamatrix.business;

import de.crazymail.datamatrix.business.manager.AttributeManager;
import de.crazymail.datamatrix.database.Sequence;
import de.crazymail.datamatrix.database.SequenceManager;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.model.Attribute;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.impl.base.filter.ExcludeRegExpPaths;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(Arquillian.class)
public class AttributeManagerTest {

    @Inject
    private AttributeManager attributeManager;
    @Inject
    private SequenceManager sequenceManager;

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive archive = ShrinkWrap.create(JavaArchive.class)
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.model")
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.database")
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.business")
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.auth")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("test-persistence.xml", "persistence.xml");
        return archive;
    }

    @Before
    public void init() {
        Sequence seq = this.sequenceManager.create(Attribute.class.getSimpleName(), "ATTR", "X", 4, 1);
        seq = this.sequenceManager.save(seq);
        this.sequenceManager.addSequence(seq);
    }

    @Test
    public void testCreateAttribute() {
        Attribute attribute = this.attributeManager.create();
        Assert.assertNotNull(attribute);

        Assert.assertNull(attribute.getId());
        Assert.assertNull(attribute.getName());
        Assert.assertNull(attribute.getNumber());
        Assert.assertNull(attribute.getDescription());
        Assert.assertNotNull(attribute.getIsArchived());
        Assert.assertFalse(attribute.getIsArchived());
    }

    /*
    @Test
    public void testSaveAndGet() throws InvalidSequenceException {
        Attribute attribute = this.attributeManager.create();
        attribute.setName("some Name");
        attribute.setDescription("some Description");
        attribute.setNumber("some Number");

        attribute = this.attributeManager.save(attribute);
        Assert.assertNotNull(attribute);
        Assert.assertNotNull(attribute.getId());

        Long attributeId = attribute.getId();

        Attribute testAttribute = this.attributeManager.get(attributeId);
        Assert.assertNotNull(testAttribute);

        Assert.assertNotNull(testAttribute.getId());
        Assert.assertEquals(attributeId, testAttribute.getId());

        Assert.assertNotNull(testAttribute.getName());
        Assert.assertEquals(attribute.getName(), testAttribute.getName());

        Assert.assertNotNull(testAttribute.getNumber());
        Assert.assertEquals(attribute.getNumber(), testAttribute.getNumber());

        Assert.assertNotNull(testAttribute.getDescription());
        Assert.assertEquals(attribute.getDescription(), testAttribute.getDescription());

        Assert.assertNotNull(testAttribute.getIsArchived());
        Assert.assertEquals(attribute.getIsArchived(), testAttribute.getIsArchived());

    }
    */

    /*
    @Test
    public void testArchive() throws InvalidSequenceException {
        Attribute attribute = this.attributeManager.create();
        attribute.setName("test name 1");
        attribute = this.attributeManager.save(attribute);

        attribute = this.attributeManager.archive(attribute);

        Assert.assertNotNull(attribute);
        Assert.assertNotNull(attribute.getName());
        Assert.assertEquals("test name 1", attribute.getName());

        Assert.assertNotNull(attribute.getId());

        Assert.assertNotNull(attribute.getIsArchived());
        Assert.assertEquals(Boolean.TRUE, attribute.getIsArchived());
    }

    */
}
