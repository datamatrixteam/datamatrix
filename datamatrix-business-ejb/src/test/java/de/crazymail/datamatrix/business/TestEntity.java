package de.crazymail.datamatrix.business;

import de.crazymail.datamatrix.model.DatamatrixElement;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "testentity_seq", sequenceName = "testentity_seq", initialValue = 5000)
public class TestEntity implements DatamatrixElement<Long> {

    @Id
    @GeneratedValue(generator = "testentity_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    private Boolean archived;
    private String name;
    private String number;

    public TestEntity() {}

    public TestEntity(String name) {
        this.name = name;
    }



    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getNumber() {
        return this.number;
    }

    @Override
    public void setNumber(String number) {
        this.number = number;
    }

    public Boolean getIsArchived() {
        return this.archived;
    }

    public void setIsArchived(Boolean archived) {
        this.archived = archived;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TestEntity{" +
                "id=" + id +
                ", archived=" + archived +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
