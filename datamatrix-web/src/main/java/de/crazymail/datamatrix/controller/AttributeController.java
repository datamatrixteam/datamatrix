package de.crazymail.datamatrix.controller;

import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.business.manager.AttributeManager;
import de.crazymail.datamatrix.model.Attribute;
import org.primefaces.event.SelectEvent;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.security.enterprise.authentication.mechanism.http.CustomFormAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.LoginToContinue;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;
import java.util.List;

@Named("attributeController")
@SessionScoped
@CustomFormAuthenticationMechanismDefinition(loginToContinue = @LoginToContinue(loginPage = "/login.jsf"))
public class AttributeController extends GenericController {

    @EJB
    private AttributeManager attributeManager;

    public AttributeController() {
        super(AttributeController.class);
    }

    private Attribute attribute;
    private Attribute selectedAttribute;

    public Attribute getSelectedAttribute() {
        return selectedAttribute;
    }

    public void setSelectedAttribute(Attribute selectedAttribute) {
        this.selectedAttribute = selectedAttribute;
    }

    public Attribute getAttribute() {
        log("getAttribute: attribute is null: " + (attribute == null));
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public void addAttribute(ActionEvent actionEvent) {
        this.attribute = this.attributeManager.create();
        log("addAttribute: attribute is null: " + (attribute == null));
        this.openDialog("addeditdialog");
    }

    public void onSelectAttribute(SelectEvent event) {
        this.update("listform:buttonaddattribute");
        this.update("listform:buttoneditattribute");
        this.update("listform:buttonarchiveattribute");
    }

    public List<Attribute> getAttributes() {
        log("getAttributes");
        return this.attributeManager.getAll();
    }

    public void saveAttribute(ActionEvent actionEvent) throws InvalidSequenceException {
        log("saveAttribute");
        try {
            this.attributeManager.save(this.attribute);
            this.closeDialog("addeditdialog");
            this.update("listform:attributesTable");
            this.attribute = null;
        } catch(Exception ex) {
            log(ex.getMessage());
        }
    }

    public void editAttribute(ActionEvent actionEvent) {
        this.attribute = this.selectedAttribute;
        this.openDialog("addeditdialog");
    }

    public void cancelAddAttribute(ActionEvent actionEvent) {
        this.attribute = null;
        this.closeDialog("addeditdialog");
    }

    public void archiveAttribute(ActionEvent actionEvent) throws InvalidSequenceException {
        this.attribute = this.selectedAttribute;
        this.attribute = this.attributeManager.archive(this.attribute);
        this.update("listform:attributesTable");
    }

    public boolean isArchivable() {
        return !(this.selectedAttribute == null);
    }

    public boolean isEditable() {
        return !(this.selectedAttribute == null);
    }

}
