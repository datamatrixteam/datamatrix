package de.crazymail.datamatrix.controller;

import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.business.manager.AttributeManager;
import de.crazymail.datamatrix.business.manager.TypeManager;
import de.crazymail.datamatrix.model.Attribute;
import de.crazymail.datamatrix.model.Type;
import de.crazymail.datamatrix.util.converter.TypeToTreeNodeConverter;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named("typeController")
@SessionScoped
public class TypeController extends GenericController {

    @Inject
    private TypeManager typeManager;

    @Inject
    private AttributeManager attributeManager;

    private TreeNode selectedTreeNode;

    private Type type;

    private List<Attribute> selectedAttributesToAdd;

    public TypeController() {
        super(TypeController.class);
    }

    public TreeNode getSelectedTreeNode() {
        return selectedTreeNode;
    }

    public void setSelectedTreeNode(TreeNode selectedTreeNode) {
        if(selectedTreeNode != null)
            log("selectedTreeNode: " + ((Type)selectedTreeNode.getData()).getName());
        else
            log("selectedTreeNode: null");

        this.selectedTreeNode = selectedTreeNode;
    }

    public List<Attribute> getSelectedAttributesToAdd() {
        return selectedAttributesToAdd;
    }

    public void setSelectedAttributesToAdd(List<Attribute> selectedAttributesToAdd) {
        this.selectedAttributesToAdd = selectedAttributesToAdd;
    }

    public List<Type> getTypes() {
        return this.typeManager.getAll();
    }

    public void addType(ActionEvent actionEvent) {
        if(this.selectedTreeNode != null)
            this.type = this.typeManager.create((Type)this.selectedTreeNode.getData());
        else
            this.type = this.typeManager.create();
        this.openDialog("addeditdialog");
    }

    public void editType(ActionEvent actionEvent) {
        this.type = (Type)this.selectedTreeNode.getData();
        this.openDialog("addeditdialog");
    }

    public void showAttributes() {
        this.update("attributesTableForm:attributesTable");
        this.openDialog("attributesdialog");
    }

    public void archiveType(ActionEvent actionEvent) throws InvalidSequenceException {
        this.type = (Type) this.selectedTreeNode.getData();
        this.type = this.typeManager.archive(this.type);
        this.update("listform:typestree");
    }

    public void closeAttributeDialog(ActionEvent actionEvent) {
        this.closeDialog("attributesdialog");
    }

    public void saveType(ActionEvent actionEvent) throws InvalidSequenceException {
        try {
            this.typeManager.save(this.type);
            this.closeDialog("addeditdialog");
            this.update("listform:typestree");
            this.type = null;
        } catch(Exception ex) {
            log(ex.getMessage());
        }
    }

    public void cancelAddType(ActionEvent actionEvent) {
        this.type = null;
        this.closeDialog("addeditdialog");
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void openAddAttributeDialog(ActionEvent actionEvent) {
        this.update("selectattributesform:attributesTable");
        this.openDialog("addattributedialog");
    }

    public List<Attribute> availableAttributes() {
        return this.attributeManager.getAll();
    }

    public void saveAttributes(ActionEvent actionEvent) throws InvalidSequenceException {
        this.log("count array elements: " + this.selectedAttributesToAdd.size());

        this.type = (Type) this.selectedTreeNode.getData();
        this.type = this.typeManager.addAttributes(this.type, this.selectedAttributesToAdd);
        this.type = this.typeManager.save(this.type);
        
        this.closeDialog("addattributedialog");
        this.update("listform:attributesTable");
       
    }

    public void cancelAddAttributes(ActionEvent actionEvent) {
        this.closeDialog("addattributedialog");
    }

    public void onSelectTypeNode(NodeSelectEvent event) {
        log("update listform:attributesTable");

        this.update("listform:attributesTable");
        this.update("listform:buttonaddtype");
        this.update("listform:buttonedittype");
        this.update("listform:buttonaddattribute");
        this.update("listform:buttonarchivetype");
    }

    public TreeNode getRootNodes() {

        List<Type> lst = this.typeManager.getRootTypes();
        TreeNode dummy = new DefaultTreeNode("some String", null);
        for(Type t : lst) {

            log("count children: " + t.getChildren().size());
            TreeNode tt = TypeToTreeNodeConverter.processTree(t, dummy);
        }



        return dummy;
    }

    public List<Attribute> getAttributesforType() {
        if(this.selectedTreeNode != null) {
            Type t = ((Type) this.selectedTreeNode.getData());
            return this.typeManager.getAttributesEffective(t);
        } else
            return new ArrayList<>();
    }

    public boolean isArchivable() {
        return !(this.selectedTreeNode == null);
    }

    public boolean isEditable() {
        return !(this.selectedTreeNode == null);
    }

    public boolean isAttributeAddable() {
        return !(this.selectedTreeNode == null);
    }
}
