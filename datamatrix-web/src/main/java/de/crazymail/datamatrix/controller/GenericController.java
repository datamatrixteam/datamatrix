package de.crazymail.datamatrix.controller;

import org.primefaces.context.RequestContext;

import java.io.Serializable;
import java.util.logging.Logger;

public abstract class GenericController implements Serializable {
    private Logger logger = null;

    public GenericController(Class clazz) {
        this.logger = Logger.getLogger(clazz.toString());
    }

    public void log(String msg) {
        this.logger.info(msg);
    }

    public void openDialog(String dialogId) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('" + dialogId + "').show();");
    }

    public void closeDialog(String dialogId) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('" + dialogId + "').hide();");
    }

    public void update(String updateId) {
        RequestContext.getCurrentInstance().update(updateId);
    }

}
