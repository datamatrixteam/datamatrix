package de.crazymail.datamatrix.util.converter;

import de.crazymail.datamatrix.model.Type;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import java.util.logging.Logger;

public class TypeToTreeNodeConverter {

    private static Logger logger = Logger.getLogger(TypeToTreeNodeConverter.class.toString());

    public static TreeNode processTree(Type rootType, TreeNode parent) {

        logger.info("processTree: create Root-Node: " + rootType.getName());

        DefaultTreeNode rootNode  = new DefaultTreeNode(rootType, parent);
        rootNode.setParent(parent);
        processNode(rootType, rootNode);
        return rootNode;
    }

    private static void processNode(Type node, TreeNode tree)  {

        logger.info("processNode: create Child-TreeNodes for: " + node.getName());

        for(Type child : node.getChildren()) {
            logger.info("processNode: actual Child-Node: " + child.getName());

            // filter archived types

            if(!child.getIsArchived()) {
                TreeNode treeNodeChild = new DefaultTreeNode(child, tree);
                treeNodeChild.setParent(tree);
                processNode(child, treeNodeChild);
            }
        }
    }
}
