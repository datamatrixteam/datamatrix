package de.crazymail.datamatrix.database;

import org.junit.Assert;
import org.junit.Test;

public class SequenceTest {

    @Test
    public void testConstructor() {
        Sequence sequence = new Sequence();
        Assert.assertNotNull(sequence);
        Assert.assertNull(sequence.getId());
        Assert.assertNull(sequence.getNumber());
        Assert.assertNull(sequence.getEntity());
        Assert.assertNull(sequence.getIsArchived());
        Assert.assertNull(sequence.getNextNumber());
        Assert.assertNull(sequence.getNumberLength());
        Assert.assertNull(sequence.getPrefix());
        Assert.assertNull(sequence.getSuffix());

    }

}
