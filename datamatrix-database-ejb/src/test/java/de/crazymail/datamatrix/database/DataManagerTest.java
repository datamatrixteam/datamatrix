package de.crazymail.datamatrix.database;

import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.database.exception.MultipleSequencesFoundException;
import de.crazymail.datamatrix.database.exception.NoSequenceFoundException;
import de.crazymail.datamatrix.model.DatamatrixElement;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.impl.base.filter.ExcludeRegExpPaths;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

@RunWith(Arquillian.class)
public class DataManagerTest {

    private static Logger log = Logger.getLogger(DataManagerTest.class.getSimpleName());

    @Deployment
    public static JavaArchive createDeployment() {
        log.info("createDeployment execution");
        JavaArchive archive = ShrinkWrap.create(JavaArchive.class)
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.database")
                .addPackages(true, new ExcludeRegExpPaths("\\/de\\/crazymail\\/datamatrix\\/.*Test\\.class"), "de.crazymail.datamatrix.model")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("test-persistence.xml", "persistence.xml");
        return archive;
    }

    @Inject
    private DataManager<TestEntity, Long> dataManager;
    @Inject
    private SequenceManager sequenceManager;
    @PersistenceContext
    private EntityManager em;

    @Before
    public void init() {
        log.info("init execution");

        dataManager.setClazz(TestEntity.class);

        try {
            Sequence seq = this.sequenceManager.getSequenceByEntity(TestEntity.class.getSimpleName());
            log.info("found sequence");
        } catch (NoSequenceFoundException e) {
            log.info("NoSequenceFoundException");
            Sequence seq = sequenceManager.create(TestEntity.class.getSimpleName(), "TE", "E", 4, 1);
            seq = sequenceManager.save(seq);
            sequenceManager.addSequence(seq);
        } catch (MultipleSequencesFoundException e) {
            log.info("MultipleSequenceException");
        } catch (Exception e) {
            log.info(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test(expected = InvalidSequenceException.class)
    @Transactional(TransactionMode.ROLLBACK)
    public void testSaveWithError() throws InvalidSequenceException {
        log.info("testSaveWithError execution");
        this.sequenceManager.deleteAll();
        this.sequenceManager.clearCache();

        TestEntity e = new TestEntity("e");
        e = this.dataManager.save(e);
    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testSaveWithoutSequence() {
        TestEntity e = new TestEntity("e");
        e = this.dataManager.saveWithoutSequence(e);
        Assert.assertNotNull(e);
        Assert.assertNotNull(e.getId());
        Assert.assertNull(e.getNumber());
        Assert.assertNotNull(e.getName());
    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testSaveAndGet() throws InvalidSequenceException {
        log.info("testSaveAndGet execution");

        TestEntity e = new TestEntity();
        e.setIsArchived(Boolean.FALSE);
        e.setName("test e");

        e = this.dataManager.save(e);
        Assert.assertNotNull(e);
        Assert.assertNotNull(e.getId());
        Assert.assertFalse(e.getIsArchived());

        Assert.assertNotNull(e.getName());
        Assert.assertEquals("test e", e.getName());

        Long entityId = e.getId();

        e = null;
        e = this.dataManager.get(entityId);

        Assert.assertNotNull(e);
        Assert.assertNotNull(e.getId());
        Assert.assertFalse(e.getIsArchived());
        Assert.assertNotNull(e.getName());
        Assert.assertEquals("test e", e.getName());

    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testGetAll() {
        try {
            this.addTestEntitiesForTest();
        } catch (InvalidSequenceException e) {
            e.printStackTrace();
        }
        List<TestEntity> all = this.dataManager.getAll();
        Assert.assertNotNull(all);
        Assert.assertEquals(25, all.size());

        Collections.sort(all, (o1, o2) -> o1.getNumber().compareTo(o2.getNumber()));

        Assert.assertEquals("e1", all.get(0).getName());
        Assert.assertEquals("e2", all.get(1).getName());
        Assert.assertEquals("e3", all.get(2).getName());
        Assert.assertEquals("e4", all.get(3).getName());
        Assert.assertEquals("e5", all.get(4).getName());
        Assert.assertEquals("e6", all.get(5).getName());
        Assert.assertEquals("e7", all.get(6).getName());
        Assert.assertEquals("e8", all.get(7).getName());
        Assert.assertEquals("e9", all.get(8).getName());
        Assert.assertEquals("e10", all.get(9).getName());
        Assert.assertEquals("e11", all.get(10).getName());
        Assert.assertEquals("e12", all.get(11).getName());
        Assert.assertEquals("e13", all.get(12).getName());
        Assert.assertEquals("e14", all.get(13).getName());
        Assert.assertEquals("e15", all.get(14).getName());
        Assert.assertEquals("e16", all.get(15).getName());
        Assert.assertEquals("e17", all.get(16).getName());
        Assert.assertEquals("e18", all.get(17).getName());
        Assert.assertEquals("e19", all.get(18).getName());
        Assert.assertEquals("e20", all.get(19).getName());
        Assert.assertEquals("e21", all.get(20).getName());
        Assert.assertEquals("e22", all.get(21).getName());
        Assert.assertEquals("e23", all.get(22).getName());
        Assert.assertEquals("e24", all.get(23).getName());
        Assert.assertEquals("e25", all.get(24).getName());
    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testNamedQueryWithoutParams() {
        TestEntity t1 = new TestEntity("testentity for unit-test");
        t1 = this.dataManager.saveWithoutSequence(t1);
        t1 = null;

        List<TestEntity> lst = this.dataManager.namedQuery(TestEntity.class, "testQuery1");
        Assert.assertNotNull(lst);
        Assert.assertEquals(1, lst.size());
        Assert.assertNotNull(lst.get(0));
        Assert.assertEquals("testentity for unit-test", lst.get(0).getName());
    }

    @Test
    @Transactional(TransactionMode.ROLLBACK)
    public void testNamedQueryWithParams() {
        TestEntity t1 = new TestEntity("testentity");
        t1 = this.dataManager.saveWithoutSequence(t1);
        t1 = null;

        TestEntity t2 = new TestEntity("testentity");
        t2 = this.dataManager.saveWithoutSequence(t2);
        t2 = null;

        List<TestEntity> lst = this.dataManager.namedQuery(TestEntity.class, "testQuery2", new HashMap<String, String>() {{put("param", "testentity");}});
        Assert.assertNotNull(lst);
        Assert.assertEquals(2, lst.size());
        Assert.assertNotNull(lst.get(0));
        Assert.assertEquals("testentity", lst.get(0).getName());
        Assert.assertEquals("testentity", lst.get(1).getName());

        lst = this.dataManager.namedQuery(TestEntity.class, "testQuery2", new HashMap<String, String>() {{put("param", "non-name");}});
        Assert.assertNotNull(lst);
        Assert.assertEquals(0, lst.size());
    }

    private void addTestEntitiesForTest() throws InvalidSequenceException {

        TestEntity e1 = new TestEntity("e1");
        e1.setIsArchived(Boolean.FALSE);
        TestEntity e2 = new TestEntity("e2");
        e2.setIsArchived(Boolean.FALSE);
        TestEntity e3 = new TestEntity("e3");
        e3.setIsArchived(Boolean.FALSE);
        TestEntity e4 = new TestEntity("e4");
        e4.setIsArchived(Boolean.FALSE);
        TestEntity e5 = new TestEntity("e5");
        e5.setIsArchived(Boolean.FALSE);
        TestEntity e6 = new TestEntity("e6");
        e6.setIsArchived(Boolean.FALSE);
        TestEntity e7 = new TestEntity("e7");
        e7.setIsArchived(Boolean.FALSE);
        TestEntity e8 = new TestEntity("e8");
        e8.setIsArchived(Boolean.FALSE);
        TestEntity e9 = new TestEntity("e9");
        e9.setIsArchived(Boolean.FALSE);
        TestEntity e10 = new TestEntity("e10");
        e10.setIsArchived(Boolean.FALSE);
        TestEntity e11 = new TestEntity("e11");
        e11.setIsArchived(Boolean.FALSE);
        TestEntity e12 = new TestEntity("e12");
        e12.setIsArchived(Boolean.FALSE);
        TestEntity e13 = new TestEntity("e13");
        e13.setIsArchived(Boolean.FALSE);
        TestEntity e14 = new TestEntity("e14");
        e14.setIsArchived(Boolean.FALSE);
        TestEntity e15 = new TestEntity("e15");
        e15.setIsArchived(Boolean.FALSE);
        TestEntity e16 = new TestEntity("e16");
        e16.setIsArchived(Boolean.FALSE);
        TestEntity e17 = new TestEntity("e17");
        e17.setIsArchived(Boolean.FALSE);
        TestEntity e18 = new TestEntity("e18");
        e18.setIsArchived(Boolean.FALSE);
        TestEntity e19 = new TestEntity("e19");
        e19.setIsArchived(Boolean.FALSE);
        TestEntity e20 = new TestEntity("e20");
        e20.setIsArchived(Boolean.FALSE);
        TestEntity e21 = new TestEntity("e21");
        e21.setIsArchived(Boolean.FALSE);
        TestEntity e22 = new TestEntity("e22");
        e22.setIsArchived(Boolean.FALSE);
        TestEntity e23 = new TestEntity("e23");
        e23.setIsArchived(Boolean.FALSE);
        TestEntity e24 = new TestEntity("e24");
        e24.setIsArchived(Boolean.FALSE);
        TestEntity e25 = new TestEntity("e25");
        e25.setIsArchived(Boolean.FALSE);

        e1 = this.dataManager.save(e1);
        e2 = this.dataManager.save(e2);
        e3 = this.dataManager.save(e3);
        e4 = this.dataManager.save(e4);
        e5 = this.dataManager.save(e5);
        e6 = this.dataManager.save(e6);
        e7 = this.dataManager.save(e7);
        e8 = this.dataManager.save(e8);
        e9 = this.dataManager.save(e9);
        e10 = this.dataManager.save(e10);
        e11 = this.dataManager.save(e11);
        e12 = this.dataManager.save(e12);
        e13 = this.dataManager.save(e13);
        e14 = this.dataManager.save(e14);
        e15 = this.dataManager.save(e15);
        e16 = this.dataManager.save(e16);
        e17 = this.dataManager.save(e17);
        e18 = this.dataManager.save(e18);
        e19 = this.dataManager.save(e19);
        e20 = this.dataManager.save(e20);
        e21 = this.dataManager.save(e21);
        e22 = this.dataManager.save(e22);
        e23 = this.dataManager.save(e23);
        e24 = this.dataManager.save(e24);
        e25 = this.dataManager.save(e25);

    }
}
