package de.crazymail.datamatrix.database.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class InvalidSequenceException extends Exception {

    public InvalidSequenceException() {
        super();
    }

    public InvalidSequenceException(Throwable e) {
        super(e);
    }
}
