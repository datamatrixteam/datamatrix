package de.crazymail.datamatrix.database.impl;

import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.database.Sequence;
import de.crazymail.datamatrix.database.SequenceManager;
import de.crazymail.datamatrix.database.exception.MultipleSequencesFoundException;
import de.crazymail.datamatrix.database.exception.NoSequenceFoundException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Singleton
public class SequenceManagerBean implements SequenceManager {

    private static Logger log = Logger.getLogger(SequenceManagerBean.class.getSimpleName());

    private Map<String, Sequence> numbersMap;

    @EJB
    private DataManager<Sequence, Long> dataManager;

    @PostConstruct
    public void init() {
        this.dataManager.setClazz(Sequence.class);
        this.numbersMap = new HashMap<>();
        List<Sequence> lst = this.dataManager.getAll();
        for(Sequence seq : lst) {
            this.numbersMap.put(seq.getEntity(), seq);
        }
    }

    @Override
    public Sequence create() {
        return new Sequence();
    }

    @Override
    public Sequence create(String clazz, String prefix, String suffix, Integer digits, Integer startNumber) {
        Sequence s = this.create();
        s.setEntity(clazz);
        s.setPrefix(prefix);
        s.setSuffix(suffix);
        s.setNumberLength(digits);
        s.setStartNumber(startNumber);
        s.setNextNumber(startNumber);

        return s;
    }

    @Override
    public Sequence save(Sequence seq) {
        log.info("save execution");
        log.info("clazz: " + seq.getEntity());
        seq = this.dataManager.saveWithoutSequence(seq);
        return seq;
    }

    @Override
    public String getNextNumber(String clazz) throws NoSequenceFoundException {
        log.info("getNextNumber execution");
        log.info("clazz: " + clazz);
        if(this.numbersMap.containsKey(clazz)) {
            String number =  this.generateNumber(this.numbersMap.get(clazz));
            this.incrementNumber(clazz);
            return number;
        } else
            throw new NoSequenceFoundException(clazz);
    }

    private String generateNumber(Sequence s) {
        return String.format("%s%0" + s.getNumberLength() + "d%s", s.getPrefix(), s.getNextNumber(), s.getSuffix());
    }

    private void incrementNumber(String clazz) {
        Sequence s = this.numbersMap.get(clazz);
        s.setNextNumber(s.getNextNumber() + 1);
    }

    @Override
    public Sequence getSequenceByEntity(String clazz) throws NoSequenceFoundException, MultipleSequencesFoundException {
        log.info("getSequenceByEntity: clazz: " + clazz);
        Map<String, String> params = new HashMap<String, String>() {{
            put("classname", clazz);
        }};
        log.info("params size: " + params.size());
        List<Sequence>  sequences = this.dataManager.namedQuery(Sequence.class, "getSequenceByEntity", params);

        log.info("sequence list size = " + sequences.size());
        if (sequences.size() == 0) {
            throw new NoSequenceFoundException(clazz);
        } else if (sequences.size() > 1) {
            throw new MultipleSequencesFoundException(clazz);
        } else {
            return sequences.get(0);
        }
    }

    @Override
    public void addSequence(Sequence s) {
        this.numbersMap.put(s.getEntity(), s);
    }

    @Override
    public void clearCache() {
        this.numbersMap = new HashMap<>();
    }

    @Override
    public void deleteAll() {
        this.dataManager.deleteSequences();
    }
}
