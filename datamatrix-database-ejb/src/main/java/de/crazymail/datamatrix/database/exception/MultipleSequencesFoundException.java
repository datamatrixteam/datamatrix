package de.crazymail.datamatrix.database.exception;

import javax.ejb.ApplicationException;

@ApplicationException
public class MultipleSequencesFoundException extends Exception {

    public MultipleSequencesFoundException() {
        super();
    }

    public MultipleSequencesFoundException(String simpleName) {
        super(simpleName);
    }
}
