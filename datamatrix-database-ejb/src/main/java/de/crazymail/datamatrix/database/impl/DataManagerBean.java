package de.crazymail.datamatrix.database.impl;

import de.crazymail.datamatrix.database.DataManager;
import de.crazymail.datamatrix.database.Sequence;
import de.crazymail.datamatrix.database.SequenceManager;
import de.crazymail.datamatrix.database.exception.InvalidSequenceException;
import de.crazymail.datamatrix.database.exception.MultipleSequencesFoundException;
import de.crazymail.datamatrix.database.exception.NoSequenceFoundException;
import de.crazymail.datamatrix.model.DatamatrixElement;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.logging.Logger;

@Stateless
public class DataManagerBean<T extends DatamatrixElement<Long>, I> implements DataManager<T, I> {

    @PersistenceContext(unitName = "de.crazymail.datamatrix")
    private EntityManager em;
    @EJB
    private SequenceManager sequenceManager;

    private Logger log = Logger.getLogger(DataManagerBean.class.getSimpleName());
    private Class clazz;

    @Override
    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    @Override
    public T save(T obj) throws InvalidSequenceException {
        log.info("save execution");
        try {
            if (obj.getId() == null) {
                log.info("id is null");
                String number = this.sequenceManager.getNextNumber(obj.getClass().getSimpleName());

                obj.setNumber(number);
                log.info("save: obj: " + obj.toString());
                this.em.persist(obj);
            } else
                obj = this.em.merge(obj);

            this.em.flush();
            return obj;

        } catch (NoSequenceFoundException e) {
            throw new InvalidSequenceException(e);
        }
    }

    @Override
    public T saveWithoutSequence(T obj) {
        if (obj.getId() == null) {
            this.em.persist(obj);
        } else
            obj = this.em.merge(obj);

        this.em.flush();
        return obj;
    }

    @Override
    public List<T> getAll() {
        String jpql = "select e from " + this.clazz.getName() + " e where e.archived = false";
        log.info("getAll: query: " + jpql);
        Query q = this.em.createQuery(jpql);
        return q.getResultList();
    }

    public T get(I id) {
        return (T) this.em.find(this.clazz, id);
    }

    public List<T> getPage(int page, int pageSize, String sortField, String sortOrder, Map<String, Object> filters) throws UnsupportedOperationException {

        //Status status = Status.ARCHIVED;
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(this.clazz);
        Root<T> root = cq.from(this.clazz);
        //javax.persistence.criteria.Path<String> p = root.get("status");
        //cq.where(cb.notEqual(p, status));
        List<Predicate> predicateList = new ArrayList<>();
        //predicateList.add(cb.notEqual(p, status));

        for (String column : filters.keySet()) {

            Object filterValue = filters.get(column);

            /*
            if (column.equals("id")) {
                try {
                    Long id = Long.valueOf((String) filterValue);
                } catch (NumberFormatException ex) {
                    throw new UnsupportedOperationException("given id is not a number");
                }
            }
            */

            /*
            if(column.equals("status")) {
                if(filterValue instanceof String) {
                    this.log("GenericDao.getPage: status is: " + filterValue);
                    filterValue = Status.valueOf((String) filterValue);
                }
            }
            */

            /*
            if(column.equals("attributeKey")) {
                if(filterValue instanceof String) {
                    //this.log("GenericDao.getPage: status is: " + filterValue);
                    //filterValue = Status.valueOf((String) filterValue);
                }
            }
            */

            Predicate predicate = null;
            String[] pathTokens = column.split("\\.");

            javax.persistence.criteria.Path<String> path = root.get(pathTokens[0]);
            for (int i = 1; i < pathTokens.length; i++) {
                path = path.get(pathTokens[i]);
            }
            if (path.getJavaType().equals(String.class)) {
                predicate = cb.like(path, "%" + filterValue + "%");
            }
            /*else if (path.getJavaType().equals(ProductionRunState.class)) {

                if(filterValue instanceof List) {
                    //predicate = cb.isTrue(path.in(filterValue));
                    //predicate = cb.isTrue(root.<ProductionRunState>get(column).in(filterValue));
                }
                else
                    predicate = cb.equal(path, filterValue);
            */
            else if (path.getJavaType().equals(Date.class)) {
                if (filterValue.toString().substring(0, 2).equals(">=")) {
                    String suffix = filterValue.toString().substring(2);
                    Date date = new Date(Long.valueOf(suffix));
                    // predicate = cb.greaterThanOrEqualTo(root.<Date>get(column), date);
                    predicate = cb.equal(path, date);

                } else if (filterValue.toString().substring(0, 2).equals("<=")) {
                    String suffix = filterValue.toString().substring(2);
                    Date date = new Date(Long.valueOf(suffix));
                    // predicate = cb.lessThanOrEqualTo(root.<Date>get(column), date);
                    predicate = cb.equal(path, date);

                } else if (filterValue.toString().substring(0, 2).equals("==")) {
                    String suffix = filterValue.toString().substring(2);
                    Date date = new Date(Long.valueOf(suffix));
                    //predicate = cb.equal(root.<Date>get(column), date);
                    predicate = cb.equal(path, date);
                }
            } else {
                predicate = cb.equal(path, filterValue);
            }
            predicateList.add(predicate);
        }

        cq.where(predicateList.toArray(new Predicate[predicateList.size()]));
        cq.select(root);
        Order order = null;
        if (sortField != null) {
            if (sortField.endsWith(".toString()")) {
                sortField = sortField.substring(0, 6);
                //log.info(sortField);
            }
            if ("ASC".equals(sortOrder)) {
                String[] pathTokens = sortField.split("\\.");
                javax.persistence.criteria.Path<String> pp = root.get(pathTokens[0]);
                for (int i = 1; i < pathTokens.length; i++) {
                    pp = pp.get(pathTokens[i]);
                }
                order = cb.asc(pp);
            }
            if ("DESC".equals(sortOrder)) {
                String[] pathTokens = sortField.split("\\.");
                javax.persistence.criteria.Path<String> pp = root.get(pathTokens[0]);
                for (int i = 1; i < pathTokens.length; i++) {
                    pp = pp.get(pathTokens[i]);
                }
                order = cb.desc(pp);
            }
            /*
            if (order == null)
                log("order is null");
            */

            cq.orderBy(order);

        }
        TypedQuery<T> q = em.createQuery(cq);
        int firstResult = (page - 1) * pageSize;
        q.setFirstResult(firstResult);
        q.setMaxResults(pageSize);
        List<T> list = q.getResultList();
        return list;
    }

    @Override
    public List<T> namedQuery(Class c, String name) {
        log.info("namedQuery: " + name);
        TypedQuery<T> q = this.em.createNamedQuery(name, c);
        return q.getResultList();
    }

    @Override
    public Integer deleteSequences() {
        Query query = this.em.createQuery("delete from Sequence");
        return query.executeUpdate();
    }

    @Override
    public List<T> getByQuery(String jpql, Map<String, String> params) {
        Query q = this.em.createQuery(jpql);
        Iterator<String> it = params.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            String value = params.get(key);
            log.info(key + " : " + value);
            q.setParameter(key, value);
        }
        return q.getResultList();
    }

    @Override
    public List<T> namedQuery(Class entity, String queryName, Map<String, String> params) {
        this.log.info("namedQuery: " + queryName);
        this.log.info("params size: " + params.size());
        TypedQuery<T> typedQuery = this.em.createNamedQuery(queryName, entity);

        Iterator<String> it = params.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            String value = params.get(key);
            log.info(key + " : " + value);
            typedQuery.setParameter(key, value);
        }

        return typedQuery.getResultList();
    }
}
