package de.crazymail.datamatrix.database.exception;

public class NoSequenceFoundException extends Exception {
    public NoSequenceFoundException(String simpleName) {
        super(simpleName);
    }
}
