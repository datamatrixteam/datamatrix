package de.crazymail.datamatrix.database;

import de.crazymail.datamatrix.model.DatamatrixElement;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(name = "getSequenceByEntity", query = "select s from Sequence s where s.entity like :classname"),
        @NamedQuery(name = "deleteSequenceByEntity", query = "delete from Sequence s where s.entity = :classname")
})

@Entity
@SequenceGenerator(name = "sequence_seq", sequenceName = "sequence_seq", initialValue = 100)
public class Sequence implements DatamatrixElement<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_seq")
    private Long id;
    private String number;

    private Boolean archived;
    private String prefix;
    private Integer numberLength;
    private String suffix;
    private Integer startNumber;
    private String entity;
    private Integer nextNumber;

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Integer getNumberLength() {
        return numberLength;
    }

    public void setNumberLength(Integer numberLength) {
        this.numberLength = numberLength;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Integer getStartNumber() {
        return startNumber;
    }

    public void setStartNumber(Integer startNumber) {
        this.startNumber = startNumber;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getNumber() {
        return this.number;
    }

    @Override
    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public Boolean getIsArchived() {
        return this.archived;
    }

    @Override
    public void setIsArchived(Boolean archived) {
        this.archived = archived;
    }

    public Integer getNextNumber() {
        return nextNumber;
    }

    public void setNextNumber(Integer nextNumber) {
        this.nextNumber = nextNumber;
    }
}
