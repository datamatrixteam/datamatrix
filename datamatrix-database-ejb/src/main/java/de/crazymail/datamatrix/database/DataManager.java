package de.crazymail.datamatrix.database;

import de.crazymail.datamatrix.database.exception.InvalidSequenceException;

import javax.ejb.Local;
import java.util.List;
import java.util.Map;

@Local
public interface DataManager<T, I>{

    void setClazz(Class clazz);

    T save(T obj) throws InvalidSequenceException;
    T saveWithoutSequence(T obj);
    T get(I id);
    List<T> namedQuery(Class clazz, String queryName);
    List<T> getAll();
    Integer deleteSequences();

    List<T> getByQuery(String jpql, Map<String, String> params);

    List<T> namedQuery(Class entity, String queryName, Map<String, String> params);
}
