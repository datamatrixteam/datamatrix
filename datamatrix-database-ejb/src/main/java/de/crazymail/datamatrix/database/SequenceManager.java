package de.crazymail.datamatrix.database;

import de.crazymail.datamatrix.database.exception.MultipleSequencesFoundException;
import de.crazymail.datamatrix.database.exception.NoSequenceFoundException;

import javax.ejb.Local;

@Local
public interface SequenceManager {

    Sequence create();
    Sequence create(String clazz, String prefix, String suffix, Integer digits, Integer startNumber);
    Sequence save(Sequence seq);
    String getNextNumber(String clazz) throws NoSequenceFoundException;
    Sequence getSequenceByEntity(String clazz) throws NoSequenceFoundException, MultipleSequencesFoundException;
    void addSequence(Sequence s);
    void clearCache();
    void deleteAll();
}
